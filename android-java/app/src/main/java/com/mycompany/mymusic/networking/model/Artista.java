package com.mycompany.mymusic.networking.model;

/**
 * Created by Rodrigo on 25/07/2018.
 */

public class Artista {

    private String id;
    private String Nome;

    public Artista(){

    }

    public Artista(String id, String nome){
        this.id = id;
        this.Nome = nome;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        id = id;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        nome = nome;
    }

}
