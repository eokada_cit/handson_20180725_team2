package com.mycompany.mymusic.networking.model;

/**
 * Created by Rodrigo on 25/07/2018.
 */

public class Usuario {

    private String id;
    private String Nome;
    private String PlaylistId;

    public Usuario(){

    }

    public Usuario(String id, String nome, String playlistId) {
        this.id = id;
        this.Nome = nome;
        this.PlaylistId = playlistId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        this.Nome = nome;
    }

    public String getPlaylistId() {
        return PlaylistId;
    }

    public void setPlaylistId(String playlistId) {
        this.PlaylistId = playlistId;
    }
}
