package com.mycompany.mymusic.networking.model;

/**
 * Created by Rodrigo on 25/07/2018.
 */

public class Musica {

    private String id;
    private String Nome;
    private String ArtistaId;
    private Artista Artista;

    public Musica(){

    }

    public Musica(String id, String nome, String artistaId) {
        this.id = id;
        this.Nome = nome;
        this.ArtistaId = artistaId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        this.Nome = nome;
    }

    public String getArtistaId() {
        return ArtistaId;
    }

    public void setArtistaId(String artistaId) {
        this.ArtistaId = artistaId;
    }

}
