package com.mycompany.mymusic.networking.service;

import com.mycompany.mymusic.networking.model.Musica;
import com.mycompany.mymusic.networking.model.Playlist;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
/**
 * Created by Rodrigo on 25/07/2018.
 */

public interface PlaylistService {

    @GET("/api/playlists?user={user}") // user obrigatorio
    Call<Playlist> obterPlaylist(@Query("user") String user);

    @PUT("/api/playlists/{playlistId}/musicas")
    Call adicionarMusicaNaPlaylist(@Path("playlistId") String playlistId, @Field("musica") ArrayList<Musica> musica);

    @DELETE ("/api/playlists/{playlistId}/musicas/{musicaId}")
    Call removerMusicaDaPlaylist(@Path("playlistId") String playlistId, @Query("musicaId") String musicaId);
}
