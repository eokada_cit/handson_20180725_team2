package com.mycompany.mymusic.networking.model;

import java.util.ArrayList;

/**
 * Created by Rodrigo on 25/07/2018.
 */

public class Playlist {

    private String id;
    private ArrayList<PlaylistMusicas> PlaylistMusicas;

    public Playlist(){

    }

    public Playlist(String id, ArrayList<PlaylistMusicas> playlistMusicas) {
        this.id = id;
        this.PlaylistMusicas = playlistMusicas;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<PlaylistMusicas> getPlaylistMusicas() {
        return PlaylistMusicas;
    }

    public void setPlaylistMusicas(ArrayList<PlaylistMusicas> playlistMusicas) {
        this.PlaylistMusicas = playlistMusicas;
    }
}
