package com.mycompany.mymusic.networking.service;

import com.mycompany.mymusic.networking.model.Musica;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Rodrigo on 25/07/2018.
 */

public interface MusicaService {

    @GET("/api/musicas?filtro={filtro}")
    Call<ArrayList<Musica>> obterTodasMusicas(@Query("filtro") String filtro);
}
