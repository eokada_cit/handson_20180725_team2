package com.mycompany.mymusic.networking;

import com.mycompany.mymusic.networking.model.Playlist;
import com.mycompany.mymusic.networking.service.PlaylistService;
import com.mycompany.mymusic.networking.service.MusicaService;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Rodrigo on 25/07/2018.
 */

public class RestApi {

    public static final String BASE_URL = "http://localhost";

    private MusicaService mMusicaService;
    private PlaylistService mPlaylistService;

    public RestApi(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mMusicaService = retrofit.create(MusicaService.class);
        mPlaylistService = retrofit.create(PlaylistService.class);
    }

    public MusicaService getMusicaService(){
        return mMusicaService;
    }

    public PlaylistService getPlaylistService(){
        return mPlaylistService;
    }
}
