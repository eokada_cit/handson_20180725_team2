package com.mycompany.mymusic;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.mycompany.mymusic.networking.RestApi;
import com.mycompany.mymusic.networking.model.Musica;
import com.mycompany.mymusic.networking.model.Playlist;
import com.mycompany.mymusic.networking.service.MusicaService;
import com.mycompany.mymusic.networking.service.PlaylistService;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    public RestApi restApi;
    public MusicaService mMusicaService;
    public PlaylistService mPlaylistService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Toast.makeText(this, "Hello World", Toast.LENGTH_SHORT).show();

        restApi = new RestApi();
        mMusicaService = restApi.getMusicaService();
        mPlaylistService = restApi.getPlaylistService();

        // API de listagem de Musicas:

        // API de Playlist de Usuário

        // API de Adicionar relação de Musicas na Playlist

        // API de Remover relação de Músicas da Playlist
    }

    /*
    public void testGetMusicas(){

        // Retorno: Array do objeto “Musica” do modelo Json
        Call<ArrayList<Musica>> call = mMusicaService.obterTodasMusicas("");
        call.enqueue(new Callback<ArrayList<Musica>>() {
            @Override
            public void onResponse(Call<ArrayList<Musica>> call, Response<ArrayList<Musica>> response) {
                // Preencher listview/recyclerview com lista de musicas
            }

            @Override
            public void onFailure(Call<ArrayList<Musica>> call, Throwable t) {

            }
        });
    }
    public void testGetPlaylist(){
        //Retorno: Objeto “Playlist” do modelo Json
        Call<Playlist> call = mPlaylistService.obterPlaylist("");
        call.enqueue(new Callback<Playlist>() {
            @Override
            public void onResponse(Call<Playlist> call, Response<Playlist> response) {

                // Mostrar musicas da playlist

            }

            @Override
            public void onFailure(Call<Playlist> call, Throwable t) {

            }
        });
    }
    public void testAdd(){
        Call<Musica> call = mPlaylistService.adicionarMusicaNaPlaylist("", new ArrayList<Musica>());
    }
    public void testRemove(){

    }
    */
}
