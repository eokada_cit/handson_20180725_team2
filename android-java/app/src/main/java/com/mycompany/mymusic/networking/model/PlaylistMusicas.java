package com.mycompany.mymusic.networking.model;

import java.util.ArrayList;

/**
 * Created by Rodrigo on 25/07/2018.
 */

public class PlaylistMusicas {

    private String PlaylistId;
    private String MusicaId;
    private Musica Musica;

    public PlaylistMusicas(){

    }

    public PlaylistMusicas(String playlistId, String musicaId, Musica musica) {
        this.PlaylistId = playlistId;
        this.MusicaId = musicaId;
        this.Musica = musica;
    }

    public String getPlaylistId() {
        return PlaylistId;
    }

    public void setPlaylistId(String playlistId) {
        this.PlaylistId = playlistId;
    }

    public String getMusicaId() {
        return MusicaId;
    }

    public void setMusicaId(String musicaId) {
        this.MusicaId = musicaId;
    }

    public Musica getMusica() {
        return Musica;
    }

    public void setMusica(Musica musica) {
        this.Musica = musica;
    }
}
